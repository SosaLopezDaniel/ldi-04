/**
 * @(#)binaHexa.java
 *
 *
 * @author 
 * @version 1.00 2014/7/13
 */


public class binaHexa {

    public binaHexa() {
    }
    public String binaHexa(Byte1 a){
    	String dato1 =a.getByte1();
    	operaciones dec= new operaciones();
    	
    	String resul1=""+dec.binarioDecimal(dato1);
    	hexaDecimal dec2= new hexaDecimal();
    	String resul2=dec2.Decimalhexa(resul1);
    	return resul2;
    	
    }
    
    public static void main (String[] args) {
    	binaHexa nuevo= new binaHexa();
    	Byte1 dato2= new Byte1();
    	dato2.ingresar("01001100");
    	String resultado=nuevo.binaHexa(dato2);
    	System.out.println("binario a hexadecimal");
    	System.out.println(resultado);
    	
}
}